#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

#########################
### Process Arguments ###
#########################
detailed=1
while getopts d: option
do
    case "${option}" in
        d)
            detailed=${OPTARG}
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done

###############################
### Environment Information ###
###############################
sudo apt-get update -y
sudo apt-get install hwinfo -y
timestamp=$(date -u +%s)
run_uuid=$(uuidgen)
nodeid=$(cat /var/emulab/boot/nodeid)
nodeuuid=$(cat /var/emulab/boot/nodeuuid)
gcc_ver=$(gcc --version | grep gcc | awk '{print $4}')

# HW info, no PCI bus on ARM means lshw doesn't have as much information
# TODO:  Add support for other sites down the road.
total_mem=$(sudo lshw -short -c memory | grep System | awk '{print $3}')
disk_size=$(sudo hwinfo --disk | grep Capacity | grep -v "0 GB" | awk '{print $2 $3}')
hw_type=$(uname -m)
if [ ${hw_type} == 'x86_64' ]; then
    mem_clock_speed=$(sudo lshw -c memory -short | grep DIMM | awk '{print $6}' | sort | uniq -d)
    mem_clock_speed=${mem_clock_speed}MHz
    disk_type="NVMe"
elif [ ${hw_type} == 'aarch64' ]; then
    mem_clock_speed="Unknown(ARM)"
    disk_type="SSD"
else
    # Temp placeholder for unknown architecture
    mem_clock_speed="Unknown(Unknown_Arch)"
    disk_type="Unknown(Unknown_Arch)"
fi

# Hash
version_hash=$(git rev-parse HEAD)


# Write to file
echo "run_uuid,timestamp,nodeid,nodeuuid,hw_type,gcc_ver,version_hash,total_mem,mem_clock_speed,disk_type,disk_size" > ~/env_out.csv
echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$hw_type,$gcc_ver,$version_hash,$total_mem,$mem_clock_speed,$disk_type,$disk_size" >> ~/env_out.csv

##############
### STREAM ###
##############
# Set up make vars
stream_ntimes=500
stream_array_size=10000000
stream_offset=0
stream_type=double
stream_optimization=O2
cd ./STREAM

# make from source and run
make clean
make NTIMES=$stream_ntimes STREAM_ARRAY_SIZE=$stream_array_size OFFSET=$stream_offset STREAM_TYPE=$stream_type OPT=$stream_optimization
./streamc
mv stream_out.csv ~/stream_out.csv

# Write to file
sed -i '1s/$/,run_uuid,timestamp,nodeid,nodeuuid,stream_ntimes,stream_array_size,stream_offset,stream_type,stream_optimization/' ~/stream_out.csv
sed -i "2s/$/,$run_uuid,$timestamp,$nodeid,$nodeuuid,$stream_ntimes,$stream_array_size,$stream_offset,$stream_type,$stream_optimization/" ~/stream_out.csv

################
### membench ###
################
# Set up make vars
membench_samples=5
membench_times=5
membench_size=1073741824LL # 1024*1024*1024, LL is required due to int overflow issues
membench_optimization=O3
cd ../membench

# make from source and run
make clean
make SAMPLES=$membench_samples TIMES=$membench_times SIZE=$membench_size OPT=$membench_optimization
./memory_profiler
mv memory_profiler_out.csv ~/membench_out.csv

# Write to file
sed -i '1s/$/,run_uuid,timestamp,nodeid,nodeuuid,membench_samples,membench_times,membench_size,membench_optimization/' ~/membench_out.csv
sed -i "2s/$/,$run_uuid,$timestamp,$nodeid,$nodeuuid,$membench_samples,$membench_times,$membench_size,$membench_optimization/" ~/membench_out.csv

###########
### FIO ###
###########
cd ~
sudo apt-get install fio -y
fio_version=$(fio -v)
if [ ${hw_type} == 'x86_64' ]; then
    disk="/dev/nvme0n1p4"
elif [ ${hw_type} == 'aarch64' ]; then
    sudo apt-get install gdisk -y
    sudo sgdisk -n 2:0:0 /dev/sda
    sudo partprobe
    disk="/dev/sda2"
else
    exit
fi

# Huge FIO header, this is the worst...
fioheader="terse_version,fio_version,jobname,groupid,error,READ_kb,READ_bandwidth,READ_IOPS,READ_runtime,READ_Slat_min,READ_Slat_max,READ_Slat_mean,READ_Slat_dev,READ_Clat_max,READ_Clat_min,READ_Clat_mean,READ_Clat_dev,READ_clat_pct01,READ_clat_pct02,READ_clat_pct03,READ_clat_pct04,READ_clat_pct05,READ_clat_pct06,READ_clat_pct07,READ_clat_pct08,READ_clat_pct09,READ_clat_pct10,READ_clat_pct11,READ_clat_pct12,READ_clat_pct13,READ_clat_pct14,READ_clat_pct15,READ_clat_pct16,READ_clat_pct17,READ_clat_pct18,READ_clat_pct19,READ_clat_pct20,READ_tlat_min,READ_lat_max,READ_lat_mean,READ_lat_dev,READ_bw_min,READ_bw_max,READ_bw_agg_pct,READ_bw_mean,READ_bw_dev,WRITE_kb,WRITE_bandwidth,WRITE_IOPS,WRITE_runtime,WRITE_Slat_min,WRITE_Slat_max,WRITE_Slat_mean,WRITE_Slat_dev,WRITE_Clat_max,WRITE_Clat_min,WRITE_Clat_mean,WRITE_Clat_dev,WRITE_clat_pct01,WRITE_clat_pct02,WRITE_clat_pct03,WRITE_clat_pct04,WRITE_clat_pct05,WRITE_clat_pct06,WRITE_clat_pct07,WRITE_clat_pct08,WRITE_clat_pct09,WRITE_clat_pct10,WRITE_clat_pct11,WRITE_clat_pct12,WRITE_clat_pct13,WRITE_clat_pct14,WRITE_clat_pct15,WRITE_clat_pct16,WRITE_clat_pct17,WRITE_clat_pct18,WRITE_clat_pct19,WRITE_clat_pct20,WRITE_tlat_min,WRITE_lat_max,WRITE_lat_mean,WRITE_lat_dev,WRITE_bw_min,WRITE_bw_max,WRITE_bw_agg_pct,WRITE_bw_mean,WRITE_bw_dev,CPU_user,CPU_sys,CPU_csw,CPU_mjf,PU_minf,iodepth_1,iodepth_2,iodepth_4,iodepth_8,iodepth_16,iodepth_32,iodepth_64,lat_2us,lat_4us,lat_10us,lat_20us,lat_50us,lat_100us,lat_250us,lat_500us,lat_750us,lat_1000us,lat_2ms,lat_4ms,lat_10ms,lat_20ms,lat_50ms,lat_100ms,lat_250ms,lat_500ms,lat_750ms,lat_1000ms,lat_2000ms,lat_over_2000ms,disk_name,disk_read_iops,disk_write_iops,disk_read_merges,disk_write_merges,disk_read_ticks,write_ticks,disk_queue_time,disk_utilization\n"

sudo blkdiscard $disk
iodepth=64
direct=1
numjobs=1
ioengine="libaio"
blocksize="4k"
# This timeout is good enough for Utah machines, however...
# Set different timeout for different sites
timeout=1440
# Run with size constraint if we're not running a detailed test
if [ ${detailed} == 1 ]; then
    size="all"
    # Sequential Write
    rw="write"
    name="fio_write_seq"
    output="$name.csv"
    sudo fio --name=$name --filename=$disk --bs=$blocksize --runtime=$timeout --iodepth=$iodepth --direct=$direct --numjobs=$numjobs --ioengine=$ioengine --rw=$rw --minimal --output=$output

    # Random Write
    rw="randwrite"
    name="fio_write_rand"
    output="$name.csv"
    sudo fio --name=$name --filename=$disk --bs=$blocksize --runtime=$timeout --iodepth=$iodepth --direct=$direct --numjobs=$numjobs --ioengine=$ioengine --rw=$rw --minimal --output=$output

    # Sequential Read
    rw="read"
    name="fio_read_seq"
    output="$name.csv"
    sudo fio --name=$name --filename=$disk --bs=$blocksize --runtime=$timeout --iodepth=$iodepth --direct=$direct --numjobs=$numjobs --ioengine=$ioengine --rw=$rw --minimal --output=$output
    
    # Random Read
    rw="randread"
    name="fio_read_rand"
    output="$name.csv"
    sudo fio --name=$name --filename=$disk --bs=$blocksize --runtime=$timeout --iodepth=$iodepth --direct=$direct --numjobs=$numjobs --ioengine=$ioengine --rw=$rw --minimal --output=$output
    
else
    size="20G"
    # Sequential Write
    rw="write"
    name="fio_write_seq"
    output="$name.csv"
    sudo fio --name=$name --filename=$disk --bs=$blocksize --size=$size --runtime=$timeout --iodepth=$iodepth --direct=$direct --numjobs=$numjobs --ioengine=$ioengine --rw=$rw --minimal --output=$output
    
    # Random Write
    rw="randwrite"
    name="fio_write_rand"
    output="$name.csv"
    sudo fio --name=$name --filename=$disk --bs=$blocksize --size=$size --runtime=$timeout --iodepth=$iodepth --direct=$direct --numjobs=$numjobs --ioengine=$ioengine --rw=$rw --minimal --output=$output
    
    # Sequential Read
    rw="read"
    name="fio_read_seq"
    output="$name.csv"
    sudo fio --name=$name --filename=$disk --bs=$blocksize --size=$size --runtime=$timeout --iodepth=$iodepth --direct=$direct --numjobs=$numjobs --ioengine=$ioengine --rw=$rw --minimal --output=$output
    
    # Random Read
    rw="randread"
    name="fio_read_rand"
    output="$name.csv"
    sudo fio --name=$name --filename=$disk --bs=$blocksize --size=$size --runtime=$timeout --iodepth=$iodepth --direct=$direct --numjobs=$numjobs --ioengine=$ioengine --rw=$rw --minimal --output=$output  
fi

output="fio_*"
sed -i 's/\;/\,/g' $output
sed -i "1s/^/$fioheader/" $output
output="fio_info.csv"
echo "run_uuid,timestamp,nodeid,nodeuuid,fio_version,fio_size,fio_iodepth,fio_direct,fio_numjobs,fio_ioengine,fio_blocksize,fio_timeout,fio_detailed" > $output
echo "$run_uuid,$timestamp,$nodeid,$nodeuuid,$fio_version,$size,$iodepth,$direct,$numjobs,$ioengine,$blocksize,$timeout,$detailed" >> $output